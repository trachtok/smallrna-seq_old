#!/bin/bash
#
# Perform miRNA alignment to miRBase using miraligner tool
# The main focus is to get good alignment without to much bias
# For quantification, it's necessary to run the postprocessing R script! 
#
# http://seqcluster.readthedocs.io/mirna_annotation.html#mirna-isomir-annotation-with-java
# http://seqcluster.readthedocs.io/mirna_annotation.html#manual-of-miraligner-java
#
# Requires: seqcluster, miraligner, crossmap.py from miraligner (optional), R, isomiRs (https://github.com/lpantano/isomiRs)
#
# TODO Merge contamination removal with the mapping script to keep miRNA reads which map with 0 mismatch but with 1 mismatch to contaminants
#
################################################################################################################
INPUT_DIR=/mnt/nfs/home/422653/000000-My_Documents/smallRNA-Seq/jirka07302018_10192018_11062018_plasma+sliny/data/contaminant_cleaned/mirna
NAME=`echo $INPUT_DIR | awk -F "/" '{print $(NF-2)}'`

OUTPUT_DIR=${INPUT_DIR%/data*}/results/miraligner
SUFFIX=".ad3trim.collapsed.mirna.prep.contamClean.fastq.gz"

MISMATCH=1 # [0, 1] Allows only 0 or 1 mismatch
ADD=3 # Max. number of additions (non-templated)
TRIM=3 # Max. number of trimmings

THREADS=3

DB=/mnt/nfs/home/422653/000000-My_Documents/human_genome/miRBase21/miraligner_DB # miRBase annotation
BOWTIE_INDEX=/mnt/nfs/home/422653/000000-My_Documents/human_genome/GRCh38/bowtie_index/Homo_sapiens.GRCh38.dna.primary_assembly # Bowtie genome index
MIRBASE=/mnt/nfs/home/422653/000000-My_Documents/human_genome/hsa_mature.fa

source activate smallRNA-seq

#source activate seqcluster
#SEQCLUSTER=$(which seqcluster)
SEQCLUSTER=$(which seqcluster)
MIRALIGNER=/mnt/nfs/home/422653/000000-My_Documents/tools/seqbuster-miraligner-3.2/modules/miraligner/miraligner.jar
#CROSSMAP=/opt/install/dir/anaconda/envs/smallRNA-seq/bin/crossmapping.py
ISORMIRS_R=`pwd`/miraligner_mirna_counts.R
MIRNA_READ_PYTHON=`pwd`/mirna_seq_tables.py

echo $SEQCLUSTER
echo $MIRALIGNER
echo $CROSSMAP

################################################################################################################
SCRATCH=/mnt/ssd/ssd_2/bioda_temp/kaja/$RANDOM
mkdir -p $SCRATCH

echo "Working directory set to $SCRATCH"
echo "########################################################################"

cp $INPUT_DIR/*$SUFFIX $SCRATCH
cp -r $DB $SCRATCH
cp -r $BOWTIE_INDEX* $SCRATCH

cd $SCRATCH

################################################################################################################
mkdir -p ./collapse

for i in *$SUFFIX
do
	echo "Processing $i"
	unpigz -c -p $THREADS $i > ${i%.gz}
	$SEQCLUSTER collapse -f ${i%.gz} -o  ./collapse
	rm ${i%.gz}
	java -jar $MIRALIGNER -freq -sub $MISMATCH -trim 3 -add 3 -minl 16 -s hsa -i ./collapse/${i%.fastq.gz}_trimmed.fastq -db $DB -o ./${i%%.*} # -pre: add sequences mapping to precursors as well; -minl is not mentioned in the manual for the tool; this option is added based on a private communication with Lorena Pantano (developer of the tool); 16 is the minimum for which is the tool designed
	#rm ./collapse/${i%.fastq.gz}_trimmed.fastq
done

for i in *.nomap
do 
	gzip $i
done &

# In case we want to remove crossmapping (an exact hit to a human genome at other positions)

#for i in *.mirna
#do
#	echo "Assignment crossmapping from sample $i"
	#python $CROSSMAP -f $i -a $BOWTIE_INDEX
#done

# Get the counts
Rscript $ISORMIRS_R `pwd` `pwd`

cd $SCRATCH/miraligner/miraligner_filtered
for i in *_filtered
do
python $MIRNA_READ_PYTHON $i $MIRBASE
done

#for i in *tsv;do sed 's/,/;/g' $i | sed 's/\t/,/g' > ${i%.tsv*}.csv;done
#for i in *csv;do unix2dos $i;done
#for i in *csv;do unoconv --format xlsx $i;done
#rm *.tsv

cd $SCRATCH
wait

mkdir -p $OUTPUT_DIR

cp -r $SCRATCH/collapse $OUTPUT_DIR
cp *.mirna *.nomap* *.mirna.cm* $OUTPUT_DIR
cp -r $SCRATCH/miraligner_seq $OUTPUT_DIR
cp -r $SCRATCH/miraligner_filtered $OUTPUT_DIR

cp gene.counts $OUTPUT_DIR

rm -r $SCRATCH
