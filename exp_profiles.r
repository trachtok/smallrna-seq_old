INPUT<-"/home/kaja/ivf_mirka24102018_DE/gene.counts"

mrcounts<-read.table(INPUT, header=TRUE, row.names=1)
#mrcounts<-mrcounts[!rownames(mrcounts) %in% c("N_ambiguous", "N_multimapping", "N_noFeature", "N_unmapped"), ] # In case we have STAR counts
#mrcounts<-mrcounts[,!colnames(mrcounts) %in% c("Chr", "Start", "End", "Strand", "Length")]# In case we have there featureCounts counts
head(mrcounts)


# Remove unwanted genes
#mrcounts<-mrcounts[!(mrcounts[,1] %in% removeGene), ] # If it's first column
#mrcounts<-mrcounts[!(rownames(mrcounts) %in% removeGene), ] # If it's rowname

#colnames(mrcounts)<-gsub("^X", "", colnames(mrcounts)) # Rename of columns if necessary
#colnames(mrcounts)<-gsub("\\..*", "", colnames(mrcounts)) # Rename of columns if necessary
#colnames(mrcounts)<-gsub("_R1.*", "", colnames(mrcounts)) # Rename of columns if necessary

# Normalize by DESeq2
library("DESeq2")
library("RColorBrewer")

coldata<-as.data.frame(as.factor(seq(1:ncol(mrcounts))))
rownames(coldata)<-colnames(mrcounts)
colnames(coldata)<-"condition"
dds<-DESeqDataSetFromMatrix(countData = mrcounts, colData = coldata, design = ~condition)

# Prefilter very low count genes
keep <- rowSums(counts(dds)) >= 10
dds <- dds[keep,]
dds$condition<-factor(dds$condition, levels=levels(coldata$condition))# Make sure the factors are correct
dds<-estimateSizeFactors(dds)

#counts <- read.table(INPUT, header=T, sep="\t", row.names = 1)
#head(counts)
#counts<-round(counts, 0)

counts<-counts(dds, normalized=TRUE) # Save normalized counts
head(counts)

removeSamples <- c("X26_PAX_SE", "X27_PAX_SE","X28_PAX_SE","X29_PAX_SE","X30_PAX_SE","X31_PAX_SE",
                   "X34_PAX_SE", "X35_PAX_SE", "X37_PAX_SE", "X38_PAX_SE", "X39_PAX_SE","KPB_PAX_SE")
removeSamples <- c("X26_Pl_SE", "X27_Pl_SE","X28_Pl_SE","X29_Pl_SE","X30_Pl_SE","X31_Pl_SE",
                   "X34_Pl_SE", "X35_Pl_SE", "X37_Pl_SE", "X38_Pl_SE", "X39_Pl_SE","KPB_Pl_SE")

counts<-counts[,!(colnames(counts) %in% removeSamples)] # Remove the samples
head(counts)

color<-rainbow(n = ncol(counts))
ncol(counts)
color<-c("darkorchid1","chocolate","deeppink","aquamarine","azure4","cornflowerblue","blue4","darkcyan","gold",
         "brown1","darkmagenta","forestgreen")
logcounts <- log(counts[,1]+1, 10) 
d <- density(logcounts)


# Get min and max for the plot
minForPlotX<-min(d$x)
maxForPlotX<-max(d$x)
maxForPlotY<-max(d$y)

# TODO Replace with recode() from dply
for (s in 2:ncol(counts)){
  logcounts <- log(counts[,s]+1, 10) 
  d <- density(logcounts)
  if(min(d$x)<minForPlotX){
    minForPlotX<-min(d$x)
  }
  if(max(d$x)>maxForPlotX){
    maxForPlotX<-max(d$x)
  }
  if(max(d$y)>maxForPlotY){
    maxForPlotY<-max(d$y)
  }
}


# Plot expression profiles
setwd("/home/kaja/ivf_mirka24102018_DE/DE_analysis/resistentvsfertile_PAX")

pdf(paste0("normalized_gene_expression_check_PAX.pdf"), width=10)
plot(d, main="Expression profiles for blood cells (PAX) samples", xlim=c(minForPlotX*1.3, maxForPlotX*1.3), ylim=c(0, maxForPlotY*1.3), xlab=paste0("Log10 of normalized 
         expression per gene (DESeq2)"), ylab="Density", col=color, lwd=2)

for (s in 2:ncol(counts)){
  logcounts <- log(counts[,s]+1, 10) 
  d <- density(logcounts)
  lines(d, col=color[s], lwd=2)
}

legend("topright", legend=colnames(counts), col = color, cex = .5, fill=color)
dev.off()
