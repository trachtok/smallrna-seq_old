#!/bin/bash
#
# Remove contamination reads from sRNA data (human)
# Allowed max. 1 mismatch by default
#
# Requires bowtie1, BBMap (filterbyname.sh), FastQC, multiQC
# 
# By default, removes "miRNA contaminants" - rRNA, tRNA, snRNA, snoRNA, YRNA, (targeted at miRNA analysis)
#	but "piRNA contaminants" can be selected as well - rRNA, tRNA, snRNA, snoRNA, YRNA, miRNA (targeted at piRNA analysis) 
#	You can choose whatever you like
#
# Based on the number of mismatches (default: 1) it iteratively maps to merged contaminant and reference sequence and removes reads mapping to the contamints
#	For example: read maps with 0 mismatches to the refence but not to the contaminatns -> kept; maps with 0 mismatches to the reference but maps to the contaminants
#		as well -> removed, ...
#
# Note: The removal of contaminants with the "itterative" steps is almost identical with just a simple bowtie1 mapping with max. XX mismatches (for example ~500 reads in 9M)
#
# Consider removing miRNAs, lincRNA, miscRNA, rRNA, snoRNA, snRNA, YRNA and tRNA https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4583685/; https://www.nature.com/articles/s42003-017-0001-7
# Should we remove also scRNA? https://www.nature.com/articles/srep37416#methods
#
# TODO - ??switch to sortMeRNA, bbduk or bloomfilter??
################################################################################
### Variables
INPUT_DIR=/mnt/nfs/home/422653/000000-My_Documents/smallRNA-Seq/jirka07302018_10192018_11062018_plasma+sliny/data/preprocessed
SEQ="mirna" # ["mirna", "pirna", ""]
INPUT_DIR=${INPUT_DIR}/$SEQ
PATH_TO_INDEX=/mnt/nfs/home/422653/000000-My_Documents/human_genome

NAME=`echo $INPUT_DIR | awk -F "/" '{print $(NF-1)}'`

OUTPUT_DIR=/mnt/nfs/home/422653/000000-My_Documents/smallRNA-Seq/jirka07302018_10192018_11062018_plasma+sliny/data/contaminant_cleaned

SUFFIX=".ad3trim.collapsed.${SEQ}.prep.fastq.gz"

THREADS=6

MISMATCH_MAX=1 # Max mismatch for end-to-end alignment

RND=$RANDOM 

# Contaminants database
if [ "$SEQ" == "pirna" ]; then

	echo "Selected piRNA-based contaminant list - rRNA, tRNA, snoRNA, snRNA, YRNA, miRNA (stemloop)."
	CONTAM_INDEX=contaminants_pirna.fasta # Merged index of rRNA, tRNA, snoRNA, snRNA, YRNA, miRNA (stemloop/pre-miRNA) - mainly for piRNA
	REF_INDEX=piRBase_v2.0.fa # piRBase or other piRNA database fasta which will be used as a reference
#	REF_NAME="piR-hsa" # Original "start" names of the reference sequences which can distinguish them from the rest
elif [ "$SEQ" == "mirna" ]; then
	echo "Selected miRNA-based contaminant list - rRNA, tRNA, snoRNA, snRNA, YRNA."
	CONTAM_INDEX=contaminants_mirna.fasta # Merged index of rRNA, tRNA, snoRNA, snRNA, YRNA - mainly for miRNA
	REF_INDEX=hsa_stemloop.fasta # miRBase or other miRNA database fasta which will be used as a reference
#	REF_NAME="hsa-" # Original "start" names of the reference sequences which can distinguish them from the rest
else
	echo "Didn't select any specific list, going for default miRNA-based contaminant list - rRNA, tRNA, snoRNA, snRNA, YRNA or custom database."
	CONTAM_INDEX=contaminants_mirna.fasta # Merged index of rRNA, tRNA, snoRNA, snRNA, YRNA - mainly for miRNA
	REF_INDEX=hsa_stemloop.fasta
#	REF_NAME="hsa-" # Original "start" names of the reference sequences which can distinguish them from the rest
fi

# In case you want to use separate database
#CONTAM_INDEX=/home/jan/Data/databases/human_genome/rRNA/human_rrna_ncbi.fasta # rRNA index
#CONTAM_INDEX=/home/jan/Data/databases/human_genome/snRNA/snRNA.fasta # snRNA index
#CONTAM_INDEX=/home/jan/Data/databases/human_genome/tRNA/hg38-mature-tRNAs.fasta # tRNA index
#CONTAM_INDEX=/home/jan/Data/databases/human_genome/snoRNA/snoRNA.fasta # snoRNA index
#CONTAM_INDEX=/home/jan/Data/databases/miRBase_v21/hsa_stemloop.fa # miRNA index
#CONTAM_INDEX=/home/jan/Data/databases/human_genome/YRNA/YRNA.fasta # YRNA index

echo "Using $CONTAM_INDEX as a contamination reference and $REF_INDEX as a reference."
echo "########################################################################"

source activate smallRNA-seq
echo "Used environment: smallRNA-seq"
echo "List of software:"
conda list

echo ""

SEED=12345

BOWTIE=$(which bowtie)
BBMAP=/opt/install/dir/anaconda/envs/smallRNA-seq/bin
FASTQC=$(which fastqc)
MULTIQC=$(which multiqc)

echo $BOWTIE
echo $BBMAP
echo $FASTQC
echo $MULTIQC

echo ""

################################################################################
### Copy inputs
SCRATCH=/mnt/ssd/ssd_2/bioda_temp/kaja/$RANDOM
mkdir -p $SCRATCH

echo "Working directory set to $SCRATCH"
echo "########################################################################"

cp $INPUT_DIR/*$SUFFIX $SCRATCH
cp $PATH_TO_INDEX/$CONTAM_INDEX $SCRATCH
cp $PATH_TO_INDEX/$REF_INDEX $SCRATCH

cd $SCRATCH

################################################################################
### Prepare merged reference for mapping

echo "Building bowtie index..."
cat $CONTAM_INDEX > ${CONTAM_INDEX%.*}.${REF_INDEX}.$RND
cat $REF_INDEX | sed 's/^>/>keepme-/g' >> ${CONTAM_INDEX%.*}.${REF_INDEX}.$RND # Add "keepme" to the reference 
${BOWTIE}-build ${CONTAM_INDEX%.*}.${REF_INDEX}.$RND ${CONTAM_INDEX%.*}.${REF_INDEX}.$RND
echo "########################################################################"
echo ""

################################################################################
### Contaminants cleaning

echo "Mapping preprocessed reads to prepared bowtie index - contamination and desired RNAs such as mirnas or pirnas..."

mkdir -p tmp
mkdir -p $SEQ

#REF_NAME="keepme-"
AWK_REF_NAME="^keepme-*" # Prepare AWK regex to filter good mappings

for i in *$SUFFIX
do
	echo "Processing sample $i"

	for MISMATCH in $(seq 0 $MISMATCH_MAX)
	do 
		echo "Mapping with $MISMATCH mismatches"

		if [ $MISMATCH -eq 0 ] # First round
		then
		
		#$BOWTIE -v $MISMATCH --maqerr 999 --tryhard --chunkmbs 2048 -a --un $OUTPUT_DIR/${i%.fastq*}.unmap.mm${MISMATCH}.fastq --time --threads $THREADS --seed $SEED ${CONTAM_INDEX%.*}.$(basename $REF_INDEX).$RND $i | grep -v $REF_NAME | uniq | gzip -c > $OUTPUT_DIR/${i%.fastq*}.delete.mm${MISMATCH}.cont.names.gz # Get unmapped reads and reads not mapping to "hsa-..."
			$BOWTIE -v $MISMATCH --maqerr 999 --tryhard --chunkmbs 2048 -a --un ./${i%.fastq*}.unmap.mm${MISMATCH}.fastq --time --threads $THREADS --seed $SEED ${CONTAM_INDEX%.*}.${REF_INDEX}.$RND $i | awk -v var="$AWK_REF_NAME" -F "\t" '{if ($3 !~ var) print $1;}' | uniq | gzip -c > ./${i%.fastq*}.delete.mm${MISMATCH}.cont.names.gz # Get unmapped reads and reads not mapping to "hsa-..."

			pigz -p $THREADS ./${i%.fastq*}.unmap.mm${MISMATCH}.fastq
		else # Every next round
			PREV_MISMATCH=$[$MISMATCH - 1]

#			$BOWTIE -v $MISMATCH --maqerr 999 --tryhard --chunkmbs 2048 -a --un $OUTPUT_DIR/${i%.fastq*}.unmap.mm${MISMATCH}.fastq --time --threads $THREADS --seed $SEED ${CONTAM_INDEX%.*}.$(basename $REF_INDEX).$RND $OUTPUT_DIR/${i%.fastq*}.unmap.mm$PREV_MISMATCH.fastq.gz | grep -v $REF_NAME | uniq | gzip -c > $OUTPUT_DIR/${i%.fastq*}.delete.mm${MISMATCH}.cont.names.gz # Get unmapped reads and reads not mapping to "hsa-..."

			$BOWTIE -v $MISMATCH --maqerr 999 --tryhard --chunkmbs 2048 -a --un ./${i%.fastq*}.unmap.mm${MISMATCH}.fastq --time --threads $THREADS --seed $SEED ${CONTAM_INDEX%.*}.${REF_INDEX}.$RND ./${i%.fastq*}.unmap.mm$PREV_MISMATCH.fastq.gz | awk -v var="$AWK_REF_NAME" -F "\t" '{if ($3 !~ var) print $1;}' | uniq | gzip -c > ./${i%.fastq*}.delete.mm${MISMATCH}.cont.names.gz # Get unmapped reads and reads not mapping to "hsa-..."

			rm ./${i%.fastq*}.unmap.mm${PREV_MISMATCH}.fastq.gz
			pigz -p $THREADS ./${i%.fastq*}.unmap.mm${MISMATCH}.fastq
		fi
	done

	# Get all read names to remove
	zcat ./${i%.fastq*}.delete.mm*.cont.names.gz | sort --parallel=$THREADS --temporary-directory=./tmp | uniq > ./${i%.fastq*}.delete.mmall.cont.names
#	unpigz -c -p $THREADS $OUTPUT_DIR/${i%.fastq*}.unmap.mm${MISMATCH}.fastq.gz | awk 'NR%4 == 1 {print $1}' | sed 's/^@//g' >> $OUTPUT_DIR/${i%.fastq*}.delete.mmall.cont.names # Get read names for the unampped reads

	rm ./${i%.fastq*}.delete.mm[[:digit:]].cont.names.gz
	rm ./${i%.fastq*}.unmap.mm${MISMATCH}.fastq.gz

	# Remove them from the original input file
	${BBMAP}/filterbyname.sh in=$i names=./${i%.fastq*}.delete.mmall.cont.names out=./${i%.fastq*}.contamClean.fastq.gz include=f -Xmx8g 1>./${i%.fastq*}.contamClean.out 2>&1 

	rm ./${i%.fastq*}.delete.mmall.cont.names
done

rm ${CONTAM_INDEX%.*}.${REF_INDEX}.$RND*
rm -r ./tmp
mv ./*.contamClean.fastq.gz ./$SEQ
mv ./*.contamClean.out ./$SEQ

wait
################################################################################
### Quality check
echo "########################################################################"
echo ""
echo "Starting quality check..."

mkdir -p ./$SEQ/fastqc

$FASTQC --outdir ./$SEQ/fastqc --threads $THREADS ./$SEQ/*.contamClean.fastq.gz

$MULTIQC --outdir ./$SEQ/fastqc ./$SEQ/fastqc/

# Number of piRNA reads after rRNA removal
cd ./$SEQ/fastqc

echo "Number of reads after rRNA removal is in $OUTPUT_DIR_QC/fastqc/preprocessed/$SEQ/contaminant_clean/total_sequences.txt"
for a in *zip
do 
	unzip -q $a
	grep "Total Sequences" ${a%.zip*}/*.txt
done > total_sequences.txt
rm -r *fastqc

wait

mkdir -p $OUTPUT_DIR

cp -r $SCRATCH/$SEQ $OUTPUT_DIR

if [ -z ${SCRATCH} ]
then
        echo "SCRATCH is unset or is empty"
else
        echo "SCRATCH is set to '$SCRATCH', deleting"
        rm -r $SCRATCH
fi

echo "Script ended."