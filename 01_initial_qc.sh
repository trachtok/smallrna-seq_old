#!/bin/bash
#PBS -l select=1:ncpus=4:mem=8gb:scratch_local=200gb
#PBS -l walltime=04:00:00
#PBS -N 01_qc_initial_run
#
# Primary QC of smallRNA-seq data
#
# Requires FastQC, Kraken (minion, swan), multiQC
#

############################################################################################
### Variables
INPUT_DIR=/mnt/nfs/home/422653/000000-My_Documents/smallRNA-Seq/jirka07302018_10192018_11062018_plasma+sliny/data/raw
OUTPUT_DIR=/mnt/nfs/home/422653/000000-My_Documents/smallRNA-Seq/jirka07302018_10192018_11062018_plasma+sliny
OUTPUT_DIR=${OUTPUT_DIR}/qc/first_qc

APPENDIX=".fastq.gz"

ADAPTERS=/mnt/nfs/home/422653/000000-My_Documents/annotations/adapters_merge.txt # Adapters to scan for in fasta format

THREADS=4

source activate smallRNA-seq

# Metacentrum modules
#module add fastQC-0.11.5
#module add python27-modules-gcc

FASTQC=$(which fastqc)
MINION=$(which minion)
SWAN=$(which swan)
MULTIQC=$(which multiqc)

# Check the tools versions
echo $FASTQC
echo $MINION
echo $SWAN
echo $MULTIQC
############################################################################################
### Copy inputs
SCRATCH=/mnt/ssd/ssd_2/bioda_temp/kaja/$RANDOM

mkdir -p $SCRATCH

echo "Working directory set to $SCRATCH"

cp $INPUT_DIR/*$APPENDIX $SCRATCH/
cp $ADAPTERS $SCRATCH/

cd $SCRATCH/

ADAPTERS=$(basename $ADAPTERS)
############################################################################################
### FastQC QC
### Minion adapter scan

mkdir -p $SCRATCH/minion

for i in *$APPENDIX
do
	echo $i
	$MINION search-adapter -i $i -show 3 -write-fasta $SCRATCH/minion/${i%.*}.minion.fasta
	$SWAN -r $ADAPTERS -q $SCRATCH/minion/${i%.*}.minion.fasta > $SCRATCH/minion/${i%.*}.minion.compare
done &

mkdir -p $SCRATCH/fastqc/raw

$FASTQC -t $THREADS -f fastq -o $SCRATCH/fastqc/raw *$APPENDIX
$MULTIQC -o $SCRATCH/fastqc/raw $SCRATCH/fastqc/raw/

cd $SCRATCH/fastqc/raw

echo "Number of reads before preprocessing is in fastqc/raw/total_sequences.txt"
for a in *zip
do
	unzip -q $a
	grep "Total Sequences" ${a%.zip*}/*.txt
done > total_sequences.txt
rm -r *fastqc

wait

# Get possible adapter sequences for all files - no checking, just taking 3rd row and/or match 
# HIGHLY EXPERIMENTAL AND NEEDS A MANUAL CHECK
for i in $SCRATCH/minion/*.compare
do
	echo $(basename $i .minion.compare) >> $SCRATCH/minion/adapters.txt
	grep -B1 -A1 "||||||||||||" $i | sed 's/^ //g' >> $SCRATCH/minion/adapters.txt
	echo -ne ">" >> $SCRATCH/minion/adapters_seq.txt
	echo $(basename $i .minion.compare) >> $SCRATCH/minion/adapters_seq.txt
	grep -A1 "||||||||||||" $i | sed 's/^ //g' | awk '{print $1}' | sed '/^||||||||||||/d'>> $SCRATCH/minion/adapters_seq.txt # Very rough extraction!
done

############################################################################################
### Copy results

mkdir -p $OUTPUT_DIR

cp -r $SCRATCH/fastqc $OUTPUT_DIR/
cp -r $SCRATCH/minion $OUTPUT_DIR/

if [ -z ${SCRATCH} ] # If variable is UNSET or HAS zero length ;if [ -z ${SCRATCH+x} ] # If variable is UNSET but CAN be zero length
then
        echo "SCRATCH is unset or is empty"
else
        echo "SCRATCH is set to '$SCRATCH', deleting"
        rm -r $SCRATCH
fi

source deactivate smallRNA-seq