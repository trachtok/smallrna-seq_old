#!/bin/bash
#PBS -l select=1:ncpus=2:mem=20gb:scratch_local=200gb
#PBS -l walltime=24:00:00
#PBS -N 02_preprocess_se
#
# Adapter and quality trimming for smallRNA-Seq for QIASEQ miRNA UMI reads
#
# Requires Cutadapt, fastx-toolkit, BBmap, FastQC, multiQC
#
# Preprocessing script designed for microRNAs+other ncRNA (probably piRNA) analysis
#
# The script does following stepts in order to preprocess data for mapping
#	1) "main" indexed adapter trimming (usually TruSeq - AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC)
#	2) Collapsing to have one read per molelule (UMIs)
#	3) "3' adapter" trimming (usually AACTGTAGGCACCATCAAT)
#	4) size filtering
#	5) quality trimming +  quality filtering -> usually unneccessary as during collapsing, all nucleotides in reads are given the best quality present in any read before collapsing, therefore, all nucleotides have almost always very good quality
#
####################################################################################################
### Variables
INPUT_DIR=/mnt/nfs/shared/Ondrej_Slaby/sequencing_results/primary_data/191217_Tana33/raw_fastq/tmp
OUTPUT_DIR=/mnt/nfs/home/422653/000000-My_Documents/smallRNA-Seq/new_pipeline_test

OUTPUT_DIR_QC=${OUTPUT_DIR}/qc/first_qc
OUTPUT_DIR=${OUTPUT_DIR}/data

ADAPTER3_SEQ1="AGATCGGAAGAGCACACGTCTGAACTCCAGTCA" # Sequence of TruSeq 3' adapter for forward reads - R1
ADAPTER3_SEQ2="AACTGTAGGCACCATCAAT" # Sequence of "real" 3' adapter right after miRNA

THREADS=16 # Number of threads to use

APPENDIX=".fastq.gz" # Files suffix to launch the analysis on

FILE_FORMAT=fastq #file format
QUALITY=33 #phred coding of input files

source activate smallRNA-seq
FASTQC=$(which fastqc)
MULTIQC=$(which multiqc)
PYTHON=$(which python)
PLOTS_R=`pwd`/lengthDis_plots.r
CUTADAPT=$(which cutadapt)

echo $FASTQC
echo $MULTIQC
echo $PYTHON
echo $PLOTS_R
echo $CUTADAPT

echo ""
####################################################################################################
### Trimming and filtering specific variables
### Cutadapt variables
ERROR_RATE=0.10 # Allowed error rate or adapters
MIN_OVERLAP=3 # 6; Minimal overlap of adapter; cutadapt uses this overlap as a "seed" and then tries to align the rest of the adapter. It doesn't remove the random hits in the middle of the sequence. We could go even lower if we would be interested in RNA/RNA fragments which could be approximately same length as the sequencing read length
QT_THRESHOLD=5 #threshold for quality trimming; we filter by number of mismatches so we need high quality reads

DISC_SHORT=15 # Discard shorter reads
NUM_ADAPT_TO_REMOVE=1 # Maximal number of adapters to remove

### Fastx-toolkit quality filtering
QF_THRESHOLD=5 #threshold for quality filtering
QF_PERC=85 #minimal percentage of bases with $QF_THRESHOLD

### Length filtering variables
MIN_LENGTH_MIR=16 # For miRNA analysis without isoform analysis is min 16 max 26 (or 28), if isomiRs should be counted try 15-29
MAX_LENGTH_MIR=28

MIN_LENGTH_PIR=26 # There are few shorter and longer piRNAs but VERY few
MAX_LENGTH_PIR=32

MIN_LENGTH=15 # General minimum length

####################################################################################################
### Adapter trimming - main indexed adapter

echo "####################################################"
echo "PART 1: adapter trimming of the rightmost 3' adapter"
echo "####################################################"
echo "Started 1st adapter trimming..."

SCRATCH=/mnt/ssd/ssd_3/temp/kaja/$RANDOM
mkdir -p $SCRATCH

echo "Working directory set to $SCRATCH"

cp $INPUT_DIR/*$APPENDIX $SCRATCH

cd $SCRATCH

mkdir -p $SCRATCH/trim_adap1
mkdir -p $SCRATCH/trim_adap1/tmp
mkdir -p $SCRATCH/cutadapt/trim_adap1

for sample in *$APPENDIX
do
	echo ""
	echo "---------------------------------------"
	echo "Started trimming" $sample

	$CUTADAPT -j $THREADS -f $FILE_FORMAT -a $ADAPTER3_SEQ1 --times $NUM_ADAPT_TO_REMOVE \
	--trim-n --max-n=0 -e $ERROR_RATE -O $MIN_OVERLAP -m $DISC_SHORT \
	-o $SCRATCH/trim_adap1/${sample%.fastq*}.ad3trim.fastq.gz --too-short-output=$SCRATCH/trim_adap1/tmp/${sample%.fastq*}.ad3short.fastq.gz \
	--untrimmed-output=$SCRATCH/trim_adap1/tmp/${sample%.fastq*}.ad3untrim.fastq.gz $sample 2>&1 | tee -a $SCRATCH/cutadapt/trim_adap1/${sample%.fastq*}.cutadapt.txt # --match-read-wildcards Matching of wildcards in the reads is also possible, but disabled by default in order to avoid matches in reads that consist of many (often low-quality) N bases;--info-file=${OUTPUT_PATH_SAVE}/${sample%.fastq*}.ad3info

	echo "Finished trimming" $sample
done
echo ""
echo "Ended 1st adapter trimming..."
echo "-----------------------------"
echo ""
echo "MultiQC of results of cutadapt."
$MULTIQC --outdir $SCRATCH/cutadapt/trim_adap1 $SCRATCH/cutadapt/trim_adap1/
echo ""

#length distribution after the first 3'end adapter removing
cd $SCRATCH/trim_adap1

mv *.ad3trim.fastq.gz $SCRATCH/trim_adap1/tmp

cd $SCRATCH/trim_adap1/tmp

for i in *.ad3short.fastq.gz
do
zcat $i | awk 'NR%4 == 2 {lengths[length($0)]++} END {for (l in lengths) {print l, lengths[l], "short"}}' > ${i%*.ad3short.fastq.gz}.lenDis.txt
zcat ${i%.ad3short.fastq.gz}.ad3trim.fastq.gz | awk 'NR%4 == 2 {lengths[length($0)]++} END {for (l in lengths) {print l, lengths[l], "trim"}}' >> ${i%.ad3short.fastq.gz}.lenDis.txt
zcat ${i%.ad3short.fastq.gz}.ad3untrim.fastq.gz | awk 'NR%4 == 2 {lengths[length($0)]++} END {for (l in lengths) {print l, lengths[l], "untrim"}}' >> ${i%.ad3short.fastq.gz}.lenDis.txt
done

echo "Plotting length distributions plots..."
for i in *.lenDis.txt
do
Rscript $PLOTS_R --file $i --trimmed 1
done

pdfunite *.pdf lenDis_index_all_adap1trimmed.pdf

mv *.ad3trim.fastq.gz $SCRATCH/trim_adap1
echo "--------------------------------------"
echo ""
####################################################################################################
### Collapsing reads
## Two tools can be used: fastx_collapser -> gives nucleotides specified quality by default, tally -> gives the best quality present in collapsed reads
echo "#########################################"
echo "PART 2: collapsing reads"
echo "#########################################"
cd $SCRATCH/trim_adap1/
mkdir -p $SCRATCH/collapsed
mkdir -p $SCRATCH/collapsedT

for sample in *.ad3trim.fastq.gz
do
	echo ""
	echo "Collapsing sample $sample."
	#echo "Running fastx_collapser..."
	#gunzip -c $sample | fastx_collapser -Q33 | reformat.sh qfake=40 in=stdin.fa out=stdout.fq | gzip -c > $SCRATCH/collapsed/$(basename $sample .fastq.gz).collapsed.fastq.gz
	echo "Running tally..."
	tally -i $sample -o $SCRATCH/collapsedT/${sample%.fastq.gz}.collapsedT.fastq.gz --with-quality
done

####################################################################################################
### Trimming 3' "helping" adapter
echo ""
echo "##################################################"
echo "PART 3: trimming of the second 3' adapter and UMIs"
echo "##################################################"
mkdir -p $SCRATCH/trim_adap2
mkdir -p $SCRATCH/cutadapt/collapsed
mkdir -p $SCRATCH/cutadapt/collapsedT


cd $SCRATCH/collapsed/

for sample in *collapsed.fastq.gz
do
#	sample=${sample}${APPENDIX}
	echo ""
	echo "------------------------------"
	echo "Started trimming" $sample

	$CUTADAPT -j $THREADS -f $FILE_FORMAT -a $ADAPTER3_SEQ2 --times $NUM_ADAPT_TO_REMOVE \
	--trim-n --max-n=0 -e $ERROR_RATE -O $MIN_OVERLAP -m $DISC_SHORT \
	-o $SCRATCH/${sample%.fastq*}.ad3trim.fastq.gz --too-short-output=$SCRATCH/trim_adap2/${sample%.fastq*}.ad3short.fastq.gz \
	--untrimmed-output=$SCRATCH/trim_adap2/${sample%.fastq*}.ad3untrim.fastq.gz $sample 2>&1 | tee -a $SCRATCH/cutadapt/collapsed/${sample%.fastq*}.cutadapt.txt # --match-read-wildcards Matching of wildcards in the reads is also possible, but disabled by default in order to avoid matches in reads that consist of many (often low-quality) N bases;--info-file=${OUTPUT_PATH_SAVE}/${sample%.fastq*}.ad3info

	echo "Finished trimming" $sample
done

cd $SCRATCH/collapsedT/

for sample in *collapsedT.fastq.gz
do
#	sample=${sample}${APPENDIX}
	echo ""
	echo "------------------------------"
	echo "Started trimming" $sample

	$CUTADAPT -f $FILE_FORMAT -j $THREADS -a $ADAPTER3_SEQ2 --times $NUM_ADAPT_TO_REMOVE \
	--trim-n --max-n=0 -e $ERROR_RATE -O $MIN_OVERLAP -m $DISC_SHORT \
	-o $SCRATCH/${sample%.fastq*}.ad3trim.fastq.gz --too-short-output=$SCRATCH/trim_adap2/${sample%.fastq*}.ad3short.fastq.gz \
	--untrimmed-output=$SCRATCH/trim_adap2/${sample%.fastq*}.ad3untrim.fastq.gz $sample 2>&1 | tee -a $SCRATCH/cutadapt/collapsedT/${sample%.fastq*}.cutadapt.txt # --match-read-wildcards Matching of wildcards in the reads is also possible, but disabled by default in order to avoid matches in reads that consist of many (often low-quality) N bases;--info-file=${OUTPUT_PATH_SAVE}/${sample%.fastq*}.ad3info

	echo "Finished trimming" $sample
done

echo ""
echo "MultiQC of results of cutadapt."
$MULTIQC --outdir $SCRATCH/cutadapt/collapsed $SCRATCH/cutadapt/collapsed/
$MULTIQC --outdir $SCRATCH/cutadapt/collapsedT $SCRATCH/cutadapt/collapsedT/

echo ""
####################################################################################################
### Size selection
# Fastx-toolkit gz input/output https://www.biostars.org/p/83237/
echo "#####################################"
echo "PART 4: size selection"
echo "#####################################"
echo ""
cd $SCRATCH/

for sample in *.ad3trim.fastq.gz
do
	# Size selection(s)
	# https://www.biostars.org/p/105428/; http://seqanswers.com/forums/showthread.php?t=31845
	echo "Size selection started for" $sample

	unpigz -c -p $THREADS $SCRATCH/$sample | paste - - - - | awk -F  "\t" -v var="$MIN_LENGTH_MIR" 'length($2) >= var' | \
	awk -F  "\t" -v var="$MAX_LENGTH_MIR" 'length($2) <= var' | tr "\t" "\n" | pigz -c -p $THREADS > $SCRATCH/${sample%.ad3trim*}.mirna.sf.fastq.gz # For miRNAs
	
	unpigz -c -p $THREADS $SCRATCH/$sample | paste - - - - | awk -F  "\t" -v var="$MIN_LENGTH_PIR" 'length($2) >= var' | \
	awk -F  "\t" -v var="$MAX_LENGTH_PIR" 'length($2) <= var' | tr "\t" "\n" | pigz -c -p $THREADS > $SCRATCH/${sample%.ad3trim*}.pirna.sf.fastq.gz # For piRNAs
	
	unpigz -c -p $THREADS $SCRATCH/$sample | paste - - - - | awk -F  "\t" -v var="$MIN_LENGTH" 'length($2) >= var' | \
	tr "\t" "\n" | pigz -c -p $THREADS > $SCRATCH/${sample%.ad3trim*}.allrna.sf.fastq.gz # All RNA, including miRNA and piRNA

	echo "Size selection finished for" $sample
done

mv $SCRATCH/*.fastq.gz $SCRATCH/trim_adap2/
mv $SCRATCH/trim_adap2/*.sf.fastq.gz $SCRATCH/

####################################################################################################
### Quality trimming
echo ""
echo "#################################################"
echo "PART 5: quality trimming"
echo "#################################################"
echo ""
echo "Started quality trimming..."
echo ""

for sample in *.mirna.sf.fastq.gz
do
	#	sample=${sample}${APPENDIX}
	echo "Started quality trimming" $sample
	$CUTADAPT -j $THREADS -q $QT_THRESHOLD,$QT_THRESHOLD -m $DISC_SHORT \
	-o $SCRATCH/${sample%.sf*}.qt.fastq.gz \
	$sample 2>&1 | tee -a $SCRATCH/cutadapt/${sample%.sf*}.cutadapt.qt.txt # --match-read-wildcards Matching of wildcards in the reads is also possible, but disabled by default in order to avoid matches in reads that consist of many (often low-quality) N bases;--info-file=${OUTPUT_PATH_SAVE}/${sample%.fastq*}.ad3info
	echo "Finished quality trimming" $sample

	echo "Quality filtering started for" $sample
	# Quality filtering
	unpigz -c -p $THREADS $SCRATCH/${sample%.sf*}.qt.fastq.gz | fastq_quality_filter \
	-Q $QUALITY -q $QF_THRESHOLD -p $QF_PERC -z -o $SCRATCH/${sample%.sf*}.prep.fastq.gz 
	echo "Quality filtering finished for" $sample

done

for sample in *.pirna.sf.fastq.gz
do
	#	sample=${sample}${APPENDIX}
	echo "Started quality trimming" $sample
	$CUTADAPT -j $THREADS -q $QT_THRESHOLD,$QT_THRESHOLD -m $DISC_SHORT \
	-o $SCRATCH/${sample%.sf*}.qt.fastq.gz \
	$sample 2>&1 | tee -a $SCRATCH/cutadapt/${sample%.sf*}.cutadapt.qt.txt # --match-read-wildcards Matching of wildcards in the reads is also possible, but disabled by default in order to avoid matches in reads that consist of many (often low-quality) N bases;--info-file=${OUTPUT_PATH_SAVE}/${sample%.fastq*}.ad3info
	echo "Finished quality trimming" $sample

	echo "Quality filtering started for" $sample
	# Quality filtering
	unpigz -c -p $THREADS $SCRATCH/${sample%.sf*}.qt.fastq.gz | fastq_quality_filter \
	-Q $QUALITY -q $QF_THRESHOLD -p $QF_PERC -z -o $SCRATCH/${sample%.sf*}.prep.fastq.gz 
	echo "Quality filtering finished for" $sample

done

for sample in *.allrna.sf.fastq.gz
do
	#	sample=${sample}${APPENDIX}
	echo "Started quality trimming" $sample
	$CUTADAPT -j $THREADS -q $QT_THRESHOLD,$QT_THRESHOLD -m $DISC_SHORT \
	-o $SCRATCH/${sample%.sf*}.qt.fastq.gz \
	$sample 2>&1 | tee -a $SCRATCH/cutadapt/${sample%.sf*}.cutadapt.qt.txt # --match-read-wildcards Matching of wildcards in the reads is also possible, but disabled by default in order to avoid matches in reads that consist of many (often low-quality) N bases;--info-file=${OUTPUT_PATH_SAVE}/${sample%.fastq*}.ad3info
	echo "Finished quality trimming" $sample

	echo "Quality filtering started for" $sample
	# Quality filtering
	unpigz -c -p $THREADS $SCRATCH/${sample%.sf*}.qt.fastq.gz | fastq_quality_filter \
	-Q $QUALITY -q $QF_THRESHOLD -p $QF_PERC -z -o $SCRATCH/${sample%.sf*}.prep.fastq.gz 
	echo "Quality filtering finished for" $sample
done

mv $SCRATCH/*.qt.fastq.gz $SCRATCH/trim_adap2/
mv $SCRATCH/*.sf.fastq.gz $SCRATCH/trim_adap2/

######################################################
### Get sequence length distributions
echo "Gettng sequence length distributions..."

mkdir -p $SCRATCH/lenDist

for sample in $SCRATCH/*.prep.fastq.gz
do
#	unpigz -c -p $THREADS $sample | awk 'NR%4 == 2 {lengths[length($0)]++} END {for (l in lengths) {print l, lengths[l]}}' > ${sample%.*}.lenDist.txt # Get sequence length distribution https://www.biostars.org/p/72433/
	readlength.sh in=$sample bin=1 nzo=f out=${sample%.*}.lenDist.txt # This is ~2-3x times faster but requires BBmap
	mv ${sample%.*}.lenDist.txt $SCRATCH/lenDist/
done

### Preprocessing QC
echo "###########################"
echo "QUALITY CONTROL"
echo ""
echo "QC of trim_adap1 reads..."

mkdir -p $SCRATCH/fastqc/preprocessed/trim_adap1

$FASTQC --outdir $SCRATCH/fastqc/preprocessed/trim_adap1 --format $FILE_FORMAT --threads $THREADS $SCRATCH/trim_adap1/*.ad3trim.fastq.gz

echo ""
echo "QC of collapsed reads..."

mkdir -p $SCRATCH/fastqc/preprocessed/collapsed

$FASTQC --outdir $SCRATCH/fastqc/preprocessed/collapsed --format $FILE_FORMAT --threads $THREADS $SCRATCH/collapsed/*.collapsed.fastq.gz


echo ""
echo "QC of trim_adap2 reads..."

mkdir -p $SCRATCH/fastqc/preprocessed/trim_adap2/ad3trim
mkdir -p $SCRATCH/fastqc/preprocessed/trim_adap2/ad3untrim
mkdir -p $SCRATCH/fastqc/preprocessed/trim_adap2/ad3short

$FASTQC --outdir $SCRATCH/fastqc/preprocessed/trim_adap2/ad3trim --format $FILE_FORMAT --threads $THREADS $SCRATCH/trim_adap2/*.ad3trim.fastq.gz
$FASTQC --outdir $SCRATCH/fastqc/preprocessed/trim_adap2/ad3untrim --format $FILE_FORMAT --threads $THREADS $SCRATCH/trim_adap2/*.ad3untrim.fastq.gz
$FASTQC --outdir $SCRATCH/fastqc/preprocessed/trim_adap2/ad3short --format $FILE_FORMAT --threads $THREADS $SCRATCH/trim_adap2/*.ad3short.fastq.gz

echo ""
echo "QC of preprocessed reads..."

mkdir -p $SCRATCH/fastqc/preprocessed

# For miRNA trimmed reads
mkdir -p $SCRATCH/fastqc/preprocessed/mirna

$FASTQC --outdir $SCRATCH/fastqc/preprocessed/mirna --format $FILE_FORMAT --threads $THREADS $SCRATCH/*.mirna.prep.fastq.gz

# For piRNA trimmed reads
mkdir -p $SCRATCH/fastqc/preprocessed/pirna

$FASTQC --outdir $SCRATCH/fastqc/preprocessed/pirna --format $FILE_FORMAT --threads $THREADS $SCRATCH/*.pirna.prep.fastq.gz

# For all RNA (all 3' adapter trimmed reads)
mkdir -p $SCRATCH/fastqc/preprocessed/allrna

$FASTQC --outdir $SCRATCH/fastqc/preprocessed/allrna --format $FILE_FORMAT --threads $THREADS $SCRATCH/*.allrna.prep.fastq.gz

$MULTIQC --outdir $SCRATCH/fastqc/preprocessed/mirna $SCRATCH/fastqc/preprocessed/mirna/
$MULTIQC --outdir $SCRATCH/fastqc/preprocessed/pirna $SCRATCH/fastqc/preprocessed/pirna/
$MULTIQC --outdir $SCRATCH/fastqc/preprocessed/allrna $SCRATCH/fastqc/preprocessed/allrna/

### Number of reads after first trimming
cd $SCRATCH/fastqc/preprocessed/trim_adap1
echo "Number of reads after first adapter trimming is in $SCRATCH/fastqc/preprocessed/trim_adap1/total_sequences.txt"

for a in *zip
do
	unzip -q $a
	grep "Total Sequences" ${a%.zip*}/*.txt
done > total_sequences.txt
rm -r *fastqc

cd $SCRATCH/fastqc/preprocessed/trim_adap2/ad3short
echo "Number of reads after second adapter trimming is in $SCRATCH/fastqc/preprocessed/trim_adap2"

for a in *zip
do
	unzip -q $a
	grep "Total Sequences" ${a%.zip*}/*.txt
done > total_sequences.txt
rm -r *fastqc

cd $SCRATCH/fastqc/preprocessed/trim_adap2/ad3trim

for a in *zip
do
	unzip -q $a
	grep "Total Sequences" ${a%.zip*}/*.txt
done > total_sequences.txt

cd $SCRATCH/fastqc/preprocessed/trim_adap2/ad3untrim

for a in *zip
do
	unzip -q $a
	grep "Total Sequences" ${a%.zip*}/*.txt
done > total_sequences.txt

### Number of reads after collapsing
cd $SCRATCH/fastqc/preprocessed/collapsed
echo "Number of reads after collapsing is in $SCRATCH/fastqc/preprocessed/collapsed/total_sequences.txt"

for a in *zip
do
        unzip -q $a
        grep "Total Sequences" ${a%.zip*}/*.txt
done > total_sequences.txt
rm -r *fastqc

### Number of miRNA reads after preprocessing
cd $SCRATCH/fastqc/preprocessed/mirna
echo "Number of reads before preprocessing is in $SCRATCH/fastqc/preprocessed/mirna/total_sequences.txt"
for a in *zip
do 
	unzip -q $a
	grep "Total Sequences" ${a%.zip*}/*.txt
done > total_sequences.txt
rm -r *fastqc

# Number of piRNA reads after preprocessing
cd $SCRATCH/fastqc/preprocessed/pirna
echo "Number of reads before preprocessing is in $SCRATCH/fastqc/preprocessed/pirna/total_sequences.txt"
for a in *zip
do 
	unzip -q $a
	grep "Total Sequences" ${a%.zip*}/*.txt
done > total_sequences.txt
rm -r *fastqc

# Number of all RNA (all 3' adapter trimmed) reads after preprocessing
cd $SCRATCH/fastqc/preprocessed/allrna
echo "Number of reads before preprocessing is in $SCRATCH/fastqc/preprocessed/allrna/total_sequences.txt"
for a in *zip
do 
	unzip -q $a
	grep "Total Sequences" ${a%.zip*}/*.txt
done > total_sequences.txt
rm -r *fastqc

### Get untrimmed reads for check
cd $SCRATCH/trim_adap2

for i in *.ad3untrim.fastq.gz
do
	unpigz -c -p $THREADS $i | fastx_collapser -Q${QUALITY} | pigz -p $THREADS > ${i%.*}.collapsed.fasta.gz # -o ${i%.*}.collapsed.fasta
done

for i in *.ad3trim.collapsed.ad3short.fastq.gz
do
zcat $i | awk 'NR%4 == 2 {lengths[length($0)]++} END {for (l in lengths) {print l, lengths[l], "short"}}' > ${i%.ad3trim.collapsed.ad3short.fastq.gz}.lenDis.txt
zcat ${i%.ad3short.fastq.gz}.ad3trim.fastq.gz | awk 'NR%4 == 2 {lengths[length($0)]++} END {for (l in lengths) {print l, lengths[l], "trim"}}' >> ${i%.ad3trim.collapsed.ad3short.fastq.gz}.lenDis.txt
zcat ${i%.ad3short.fastq.gz}.ad3untrim.fastq.gz | awk 'NR%4 == 2 {lengths[length($0)]++} END {for (l in lengths) {print l, lengths[l], "untrim"}}' >> ${i%.ad3trim.collapsed.ad3short.fastq.gz}.lenDis.txt
done

for i in *.lenDis.txt
do
Rscript $PLOTS_R --file $i --trimmed 2
done

pdfunite *.pdf lenDis_index_all_adap2UMItrimmed.pdf

cd $SCRATCH

## Clean
mkdir -p $SCRATCH/mirna
mkdir -p $SCRATCH/pirna
mkdir -p $SCRATCH/allrna

mv $SCRATCH/*.mirna.prep.fastq.gz $SCRATCH/mirna
mv $SCRATCH/*.pirna.prep.fastq.gz $SCRATCH/pirna
mv $SCRATCH/*.allrna.prep.fastq.gz $SCRATCH/allrna

#mkdir -p $OUTPUT_DIR_QC

cp -r $SCRATCH/mirna $OUTPUT_DIR
cp -r $SCRATCH/pirna $OUTPUT_DIR
cp -r $SCRATCH/allrna $OUTPUT_DIR
cp -r $SCRATCH/trim_adap2 $OUTPUT_DIR
cp -r $SCRATCH/trim_adap1 $OUTPUT_DIR
cp -r $SCRATCH/collapsed $OUTPUT_DIR
cp -r $SCRATCH/fastqc/preprocessed $OUTPUT_DIR_QC

mkdir -p $OUTPUT_DIR/mirna
mv $OUTPUT_DIR/*.mirna.prep.fastq.gz $OUTPUT_DIR/mirna

#rm -r $SCRATCH
