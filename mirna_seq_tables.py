#Script that will read ouput of miraligner and output table of different mirna reads for each mirna with frequence and canonical/isoform information
#
# INPUT1 = .mirna file from miraligner
# INPUT2 =  miRBase mature miRNA fasta
#
# for i in *_filtered; do python mirna_seq_tables.py $i hsa_mature.fa; done
#######################################################################################################################################################
import os
import sys

f1 = open(sys.argv[1], "r") #file with miraligner filtered results, firts 3 columns are: seq, name, freq

dic = {} #dictionary to store miRNA name and all sequences, miRNA : [read1_x1,read2_10x,read3_5x,...]

for line in f1:
    if "mism" in line:
        continue
    else:
        l1 = line.split()
        if l1[1] not in dic.keys():
            tmp = []
            tmp.append(l1[0]+"_"+l1[-1]+"x")
            dic[l1[1]] = tmp
        else:
            tmp = []
            tmp = dic[l1[1]]
            tmp.append(l1[0]+"_"+l1[-1]+"x")
            dic[l1[1]] = tmp

f1.close()


f2 = open(sys.argv[2], "r") #file with mature miRNA sequences

dic_mature = {} #miRNA : mature sequence

for line in f2:
    if ">" in line:
        l1 = line.split()
        name = ""
        name = l1[0].strip(">")
    else:
        seq = ""
        seq = line.strip("\n")
        seq = seq.replace("U","T")
        dic_mature[name] = seq

f2.close()

#NOT USED - initially, I wanted to add a stem-loop sequence as well, but since they will open it in excel, it would not be useful for them
#f3 = open(sys.argv[3], "r")  #file with pre-miRNA sequences
#
#dic_stem = {} # miRNA : stem_loop sequence
#
#for line in f3:
#    if ">" in line:
#        l1 = line.split()
#        name = ""
#        name = l1[0].strip(">")
#    else:
#        seq = ""
#        seq = line.strip("\n")
#        seq = seq.replace("U","T")
#        dic_stem[name] = seq
#
#f3.close()

#name of the output file in form PBAX910_SE_read_seq.tsv
fname = sys.argv[1].split("_filtered")[0]+"_read_seq.tsv"

fout = open(fname, "w")

t = "\t"
fout.write("miRNA"+t+"mature"+t+"read_seq"+t+"read_frequency"+t+"read_type"+"\n")

tmp = {}
for key, value in dic.iteritems():
    mirna = key #name of mature miRNA
    #mirna_stem = key.replace("R","r").strip("-5p").strip("-3p") #name of stem loop miRNA
    if mirna in dic_mature.keys():
        seq = dic[mirna][0].split("_")[0] #sequence of mirna read
        freq = dic[mirna][0].split("_")[1].strip(".x") #frequency
        seq_mature = "" #sequence of mature miRNA = canonical sequence
        seq_mature = dic_mature[mirna]
        if seq_mature == seq:
            fout.write(mirna+t+dic_mature[mirna]+t+seq+t+freq+t+"canonical"+"\n")
        else:
            fout.write(mirna+t+dic_mature[mirna]+t+seq+t+freq+t+"isomiRNA"+"\n")
        for item in value[1:]: #loop through all mirna reads and write one read per line
            seq = ""
            seq = item.split("_")[0]
            freq = item.split("_")[1].strip(".x")
            if seq_mature == seq:
                fout.write("-"+t+dic_mature[mirna]+t+seq+t+freq+t+"canonical"+"\n")
            else:
                fout.write("-"+t+dic_mature[mirna]+t+seq+t+freq+t+"isomiRNA"+"\n")

    else: #remove this else loop when you synchronize miraligner DB and miRBase fasta file
        seq = dic[mirna][0].split("_")[0]
        freq = dic[mirna][0].split("_")[1].strip(".x")
        seq_mature = "-"
        fout.write(mirna+t+"-"+t+seq+t+freq+t+"isomiRNA"+"\n")
        for item in value[1:]:
            seq = ""
            seq = item.split("_")[0]
            freq = item.split("_")[1].strip(".x")
            fout.write("-"+t+"-"+t+seq+t+freq+t+"isomiRNA"+"\n")

fout.close()

# for i in *tsv;do sed 's/,/;/g' $i | sed 's/\t/,/g' > ${i%.tsv*}.csv;done
# for i in *csv;do unix2dos $i;done
# for i in *csv;do unoconv --format xlsx $i;done
