#!/usr/bin/env Rscript

library("ggplot2")
library("optparse")

option_list = list(
  make_option(c("-f", "--file"), type="character", default=NULL,
              help="dataset file name", metavar="character"),
  make_option(c("-o", "--out"), type="character", default="out.txt",
              help="output file name [default= %default]", metavar="character"),
  make_option(c("-t", "--trimmed"), type="integer", default=1, help="reads with adap1 trimmed [1] or reads with both adap1 and adap2+UMI trimmed [2], default=1", metavar="integer")
);

opt_parser = OptionParser(option_list=option_list);
opt = parse_args(opt_parser);

if (is.null(opt$file)){
  print_help(opt_parser)
  stop("At least one argument must be supplied (input file).n", call.=FALSE)
}

name <- substr(opt$file, 1, nchar(opt$file)-11)
input <- read.table(opt$file, sep=" ", header=F)

def<-c("adapter2+UMI","miRNAs","53","31")
def[opt$trimmed == 1]<-c("adapter2+UMI","miRNAs","53","31")
def[opt$trimmed == 2]<-c("miRNAs","piRNAs","28","21")

def[1]
def[2]

# Change the color

pdf(file=paste(name,"pdf",sep="."), width = 16, height=4)
ggplot(data=input, aes(x=input$V1, y=input$V2, group=input$V3)) +
  geom_line(aes(color=input$V3))+
  geom_point(aes(color=input$V3))+
  scale_x_continuous(breaks=input$V1)+
  labs(x="basepairs",y="counts",title="Length distribution of all reads after preprocessing",subtitle=name)+
  theme_bw()+
  geom_vline(aes(xintercept = as.integer(def[4]), color=def[1]), show.legend=TRUE)+
  geom_vline(aes(xintercept = as.integer(def[3]), color=def[2]), show.legend=TRUE)+
  scale_color_manual(name="Most common length of: ",values=c("black","grey","dodgerblue4","dodgerblue","deepskyblue"))+
  theme(legend.position="top")
#  scale_fill_manual(values=c("#CC0000", "#006600", "#669999", "#00CCCC","#660099"))
